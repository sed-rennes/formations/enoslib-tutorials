from enoslib.api import play_on, wait_ssh
from enoslib.infra.enos_vmong5k.provider import VMonG5k
from enoslib.infra.enos_vmong5k.configuration import Configuration

import logging
import os

logging.basicConfig(level=logging.DEBUG)

CLUSTER = "paravance"

# path to the inventory
inventory = os.path.join(os.getcwd(), "hosts")

# claim the resources
conf = Configuration.from_settings(job_name="enoslib_tutorial")\
                    .add_machine(roles=["server"],
                                 cluster=CLUSTER,
                                 number=5,
                                 flavour="large")\
                    .add_machine(roles=["client"],
                                 cluster=CLUSTER,
                                 number=5,
                                 flavour="medium")\
                    .finalize()

provider = VMonG5k(conf)

roles, networks = provider.init()
wait_ssh(roles)


servers = roles["server"]
clients = roles["client"]

for s, c in zip(servers, clients):
    c.extra.update(target=s.address)

with play_on(roles=roles) as p:
    p.apt(name=["iperf3", "tmux"], state="present")

with play_on(pattern_hosts="server", roles=roles) as p:
    p.shell("tmux new-session -d 'exec iperf3 -s'")

with play_on(pattern_hosts="client", roles=roles) as p:
    p.shell("iperf3 -c {{ target }} -t 30 --logfile iperf3.out")
    p.fetch(src="iperf3.out", dest="iperf3.out")
