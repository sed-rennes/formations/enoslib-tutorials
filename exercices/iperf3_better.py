from enoslib.api import run_command, wait_ssh
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_g5k.configuration import Configuration, NetworkConfiguration
from enoslib.service import Monitoring

import logging


logging.basicConfig(level=logging.INFO)


SITE = "rennes"
CLUSTER = "paravance"

network = NetworkConfiguration(id="n1",
                               type="prod",
                               roles=["my_network"],
                               site=SITE)
private = NetworkConfiguration(id="n2",
                               type="kavlan",
                               roles=["private"],
                               site=SITE)

conf = Configuration.from_settings(job_name="enoslib_tutorial",
                                   job_type="deploy")\
                    .add_network_conf(network)\
                    .add_network_conf(private)\
                    .add_machine(roles=["server"],
                                 cluster=CLUSTER,
                                 nodes=1,
                                 primary_network=network,
                                 secondary_networks=[private])\
                    .add_machine(roles=["client"],
                                 cluster=CLUSTER,
                                 nodes=1,
                                 primary_network=network,
                                 secondary_networks=[private])\
                    .finalize()

provider = G5k(conf)
roles, networks =  provider.init()
wait_ssh(roles)

def pprint(d):
    import json
    print(json.dumps(d, indent=4))


discover_networks(roles, networks)

m = Monitoring(collector=roles["server"],
               agent=roles["server"] + roles["client"],
               ui=roles["server"],
               network="private")
m.deploy()

server = roles["server"][0]
run_command("apt update && apt install -y iperf3 tmux", roles=roles)
run_command("tmux new-session -d 'exec iperf3 -s'", pattern_hosts="server", roles=roles)
result = run_command(f"iperf3 -c {server.address} --json -t 120", pattern_hosts="client", roles=roles)
pprint(result)
