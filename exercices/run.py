from enoslib.api import run, run_command, gather_facts
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_g5k.configuration import Configuration, NetworkConfiguration

import logging


logging.basicConfig(level=logging.INFO)


SITE = "rennes"
CLUSTER = "paravance"

network = NetworkConfiguration(id="n1",
                               type="prod",
                               roles=["my_network"],
                               site=SITE)

conf = Configuration.from_settings(job_name="enoslib_tutorial",
                                   job_type="allow_classic_ssh")\
                    .add_network_conf(network)\
                    .add_machine(roles=["server"],
                                 cluster=CLUSTER,
                                 nodes=1,
                                 primary_network=network)\
                    .add_machine(roles=["client"],
                                 cluster=CLUSTER,
                                 nodes=1,
                                 primary_network=network)\
                    .finalize()

provider = G5k(conf)
roles, networks =  provider.init()


def pprint(d):
    import json
    print(json.dumps(d, indent=4))


server = roles["server"][0]
# ---
# Using run
# --------------------------------------------------------------------
result = run(f"ping -c 5 {server.address}", roles["client"])
pprint(result)


# ---
# Using run_command 1/2
# --------------------------------------------------------------------
result = run_command(f"ping -c 5 {server.address}",
                     pattern_hosts="client",
                     roles=roles)
pprint(result)


# ---
# Gather facts
# --------------------------------------------------------------------
result = gather_facts(roles=roles)
pprint(result)


# ---
# Passing a variable to the Ansible World using a global level variable
# --------------------------------------------------------------------
server = roles["server"][0]
extra_vars={"server_ip": server.address}
result = run("ping -c 5 {{ server_ip }}", roles["client"], extra_vars=extra_vars)


# ---
# Passing a variable to the Ansible World using a host level variable
# --------------------------------------------------------------------
server = roles["server"][0]
client = roles["client"][0]
client.extra.update(server_ip=server.address)
result = run("ping -c 5 {{ server_ip }}", roles["client"])


# ---
# Cross ping using host variables
# --------------------------------------------------------------------
server = roles["server"][0]
client = roles["client"][0]
client.extra.update(other=server.address)
server.extra.update(other=client.address)
result = run("ping -c 5 {{ other }}", roles["client"] + roles["server"])
pprint(result)
result = run_command("ping -c 5 {{ other }}", roles=roles)
pprint(result)
