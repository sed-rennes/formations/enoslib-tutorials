#+TITLE: Distributed experiments on Grid'5000 ... and beyond !
#+DATE: 
#+AUTHOR: Matthieu Simonin

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="timeline.css" />

#+MACRO: enoslib EnOSlib
#+MACRO: src_host https://gitlab.inria.fr/discovery/enoslib/blob/v4.8.1/enoslib/host.py#L8-14
#+MACRO: doc_external_access https://discovery.gitlabpages.inria.fr/enoslib/tutorials/grid5000.html#accessing-http-services-inside-grid-5000
#+MACRO: src_provider https://gitlab.inria.fr/discovery/enoslib/tree/v4.8.1/enoslib/infra
#+MACRO: doc_provider https://discovery.gitlabpages.inria.fr/enoslib/apidoc/infra.html
#+MACRO: doc_tasks https://discovery.gitlabpages.inria.fr/enoslib/tutorials/using-tasks.html
#+MACRO: doc_g5k_schema https://discovery.gitlabpages.inria.fr/enoslib/apidoc/infra.html#g5k-schema
#+MACRO: doc_api https://discovery.gitlabpages.inria.fr/enoslib/apidoc/api.html
#+MACRO: doc_services https://discovery.gitlabpages.inria.fr/enoslib/apidoc/service.html

* Foreword

** Existing tools (Grid'5000)

   - {{{enoslib}}} falls under the **Experiment management tools** of the following
     list:
     https://www.grid5000.fr/w/Grid5000:Software

   - {{{enoslib}}} can target Grid'5000 but also other testbeds (Chameleon, local machines...)

   - {{{enoslib}}} provides high level constructs to help you with your experiments

** EnOSlib quicktour
   
  - Documentation: https://discovery.gitlabpages.inria.fr/enoslib/index.html 
  - Source:  https://gitlab.inria.fr/discovery/enoslib
  - Reach us on:
      + https://framateam.org/enoslib
      + https://gitlab.inria.fr/discovery/enoslib/issues

** Contributing
  
   *Before experimenting*

   - Tell us what your plans are:
     + There might be already users doing similar thing
     + There might be some missing/hidden pieces in the library you might need
       
   *While experimenting*
   
   - Write bug reports / ask questions
   - Fix bugs / add your features

   *After experimenting*

   - Give your feedback
   - Add yourself to the list: https://discovery.gitlabpages.inria.fr/enoslib/theyuseit.html

* Setup on Grid'5000

Connect to a Grid'5000 frontend of your choice.

- create a new directory to host all the scripts of the session
- bootstrap a new python3 virtualenv 
- install {{{enoslib}}} and configure the access to the API
- you'll also want to have ipython and ipdb installed

#+BEGIN_SRC bash :noeval
$frontend: mkdir enoslib_seminar
$frontend: cd enoslib_seminar
$frontend: virtualenv --python=python3 venv
$frontend: source venv/bin/activate
$frontend(venv): pip install enoslib ipython ipdb
$frontend(venv): echo '
verify_ssl: False
' > ~/.python-grid5000.yaml
#+END_SRC

* Introduction 1/2: {{{enoslib}}} warmup on Grid'5000
  
  Learn how to get 2 nodes from Grid'5000 and start launching remote commands.

** Reserve 2 nodes
   
   #+BEGIN_note
   With {{{enoslib}}} you first describe your resource requirements using an abstract
   resource description. 
   Note that the network should be explictly stated.
   #+END_note

   Write the following python script in a file ~run.py~. If needed adapt the
   ~CLUSTER~ and ~SITE~ variables.

   #+INCLUDE: exercices/run.py :lines "1-35" src python

   For the sake of curiosity let's inspect the roles and networks data
   structures using ipython.

#+BEGIN_SRC bash :noeval
$frontend (venv): ipython
In [1]: run run.py
# ...
# ...
In [2]: roles
# ...
In [3]: networks
#+END_SRC

    #+BEGIN_note
    The abstract resource description is concretized by the call to the
    ~provider.init~ method. ~roles~ and ~networks~ contains the concrete machines
    and networks given by Grid'5000.
    Check the attributes of the Host data structure in the code: {{{src_host}}}
    #+END_note

   For the following you have two choices to run the examples:
   - (prefered) append it in the previous file and re-run the file (yes this is safe to do so)
   - write the example in the previously opened ipython console

** Discover the ~run~ command and its variants

    Before proceeding you can add this util function to your code. It is only
    used to pretty print a python dictionnary.
    #+INCLUDE: exercices/run.py :lines "35-39" src python

    And use the ~enoslib.api.run~ function 
    #+INCLUDE: exercices/run.py :lines "40-47" src python
   
    Or the ~enoslib.api.run_command~ function
    #+INCLUDE: exercices/run.py :lines "48-56" src python

    #+BEGIN_note
    ~enoslib.api.run~ is a specialisation of ~enoslib.api.run_command~. 
    The latter let's you use [[https://docs.ansible.com/ansible/latest/user_guide/intro_patterns.html][some fancy patterns]] to determine the list of hosts to run the command on.

    And yes, it uses Ansible behind the scene.
    #+END_note

** Advanced usages
    
   #+BEGIN_note
   For all the remote interactions, {{{enoslib}}} relies on [[https://docs.ansible.com/ansible/latest/index.html][Ansible]]. Ansible
   has it own variables management system.
   For instance the task ~Gather Facts~ at the beginning of the previous tasks
   gathers informations about all/some remote hosts and store them in the
   Ansible management system.
   #+END_note

   Let's see what Ansible is gathering about the hosts:

   #+INCLUDE: exercices/run.py :lines "58-65" src python

   #+BEGIN_note
   {{{enoslib}}} sits in between two worlds: the Python world and the Ansible
   world. One common need is to pass a variables from one world to another.
   - ~enoslib.api.gather_facts~ is a way to get, in Python, the variables known
     by Ansible about each host.
   - ~extra_vars~ keyword argument of ~enoslib.api.run~ or ~enoslib.api.run_command~ will 
     pass variables from Python world to Ansible world (global variable)
   - Injecting a key/value in a ~Host.extra~ attribute will make the variable ~key~ available to Ansible.
     This makes the variables Host specific.
   #+END_note

   The following inject a global variable in the Ansible world
   #+INCLUDE: exercices/run.py :lines "65-71" src python

** Ninja level

   The following is valid and inject in the ~client~ host a specific variable to
   keep track of the server IP.

   #+INCLUDE: exercices/run.py :lines "73-81" src python

   #+BEGIN_note
   Host level variables are interesting to introduce some dissymetry between
   hosts while still using one single command to reach all of them.
   #+END_note

   #+BEGIN_question
   How to perform simultaneously the ping to the other machine in calling only
   once ~run~ or ~run_command~ and using host level variables?
   #+END_question

** Putting all together
   Access the full file: [[file:exercices/run.py]]

** Some references

   - G5k configuration schema: {{{doc_g5k_schema}}}
   - API Reference: {{{doc_api}}}

* Introduction 2/2: Iperf3 playground 

  Let's experiment with [[https://iperf.fr/][iperf3]]: a network bandwidth measuring tool. The goal is
  to deploy a simple benchmark between two hosts. 

  We'll also instrument the deployment in order to visualize in real-time the
  network traffic between the hosts. Since this is super common, {{{enoslib}}}
  exposes a /monitoring service/ that lets you deploy very quickly what is
  needed.
  
** First attempt
   
   We adapt the previous example in the following script:
   #+INCLUDE: exercices/iperf3.py src python

   Now, let's visualize the network traffic in real-time !
   #+BEGIN_note
   Usually I follow this to access services running inside Grid'5000:
   {{{doc_external_access}}}

   So to access the monitoring dashboard you need to connect using your browser
   to the ~server~ host on the port 3000.
   #+END_note
   
   You should be able to visualize such a thing (after a bit of point and clicks):
   - First, add a data source. In this setting {{{enoslib}}} is using ~InfluxDB~ 
     on this address ~http://localhost:8086~ and uses the ~telegraf~ database
   - Then, since part of the experimenter work also consists in analysing the 
     data, write the right request to monitor the traffic (check the Fig. [[fig:iperf3]])

   #+CAPTION: iperf3 / monitoring
   #+NAME:   fig:iperf3
   #+ATTR_HTML: :width 100% :style border:1px solid black;
   [[file:figs/iperf3.png][file:figs/iperf3.png]]
   
** Discussion
   
   Let's discuss about:

   - What is good 
   - What is wrong

** A better approach (maybe)
    Access the full file: [[file:exercices/iperf3_better.py]]

** Ninja level

   Add the [[https://discovery.gitlabpages.inria.fr/enoslib/apidoc/service.html#skydive][Skydive]] service to your deployment. 
   It should be accessible on the port ~8082~ of the analyzer node. You should get something like:

   [[file:figs/skydive_enoslib.png]]
   
** Some references
   
   - Services: {{{doc_services}}}
   
* Modules: for safer remote actions
  
  In this section we'll discover the idiomatic way of managing resources on the
  remote hosts. A resource can be anything: a user, a file, a line in a file, a
  repo on Gitlab, a firewall rule ...


** Idempotency

  Let's assume you want to create a user (~foo~). With the ~run_command~ this would look like:

  #+BEGIN_SRC python :noeval 
  run_command("useradd -m foo", roles=role)
  #+END_SRC

  The main issue with this code is that it is not *idempotent*. Running it once
  will applied the effect (create the user). But, as soon as the user exist in
  the system, this will raise an error.

** One reason why idempotency is important

  Let's consider the following snippet (mispelling the second command is intentional)
  #+BEGIN_SRC python :noeval 
  run_command("useradd -m foo", roles=role)
  run_command("mkdirz plop")
  #+END_SRC
  Executing the above leads the system with the user ~foo~ created but the the
  directory ~plop~ not created since the second command fails.

  So what you want to do is to fix the second command and re-run the snippet again.
  But, you can't do that because ~useradd~ isn't idempotent.

** Idempotency trick

   One easy solution is to protect your call to non idempotent commands with
   some ad'hoc tricks

   Here it can look like this:

   #+BEGIN_SRC python :noeval 
   run_command("id foo || useradd -m foo", roles=role)
   run_command("mkdir -p plop")
   #+END_SRC

   *What's wrong with that*

   - The trick depends on the command
   - Re-reading the code is more complex: the code focus on the **how** not the **what**

** General idempotency
    
   The idiomatic solution is to use modules (inherited from the Ansible
   Modules). The modules are specified in a *declarative* way and they ensure
   *idempotency* for most of them.

   So rewriting the example with modules looks like:
   #+BEGIN_SRC python :noeval 
   with play_on(roles=roles) as p:
       p.user(name="foo", state="present", create_home="yes")
       p.file(name="plop", state="directory")
   #+END_SRC
   
   ~enoslib.api.play_on~ is the entry point to the module system.

   You can run this code as many times as you want without any error. You'll
   eventually find one user ~foo~ and one directory ~plop~ in your target
   systems.

   They are more than 2500 modules: https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html

   If you can't find what you want you must know that:
   - Writing your own module is possible
   - Falling back to the idempotency trick is reasonable

* Providers: to replicate your experiment

  #+BEGIN_note
  The resources that are used for your experiment are acquired through a
  provider. Providers are a mean to decouple the infrastructure code (the code
  that gets the resources) from the code that runs the experiment. Changing the
  provider allows to replicate the experiment on another testbed.
  #+END_note

  Originally it was used to iterate on the code locally (using the Vagrant
  provider) and to only test on Grid'5000 when necessary.

  We now have couple of providers that you may picked or mixed.

** iperf3 on virtual machines on Grid'5000

   We'll adapt the initial iperf3 example to use virtual machines instead of
   bare-metal machine. 

   Note that:
   
   - The configuration object is different
   - The experimentation logic is the same (rewritten using modules when it applies)
   
   #+INCLUDE: exercices/iperf3_vms.py src python

   Using module using the ~play_on~ context manager does not bring back the
   results of the commands. Iperf3 let's you write the result of the command on
   a file. We just need to scp the file back to our local machine using the
   ~fetch~ module.

** Ninja level 
   
   #+BEGIN_question
   Creates 5 ~server~ machines and 5 ~client~ machines and start 5 *parallel*
   streams of data using ~iperf3~.
   #+END_question

** References

   - Doc: {{{doc_provider}}} 
   - Sources: {{{src_provider}}}

* Tasks: to organize your experiment

  To discover the Task API, head to {{{doc_tasks}}}.

  The examples are written for Vagrant but may be changed to whatever provider you like/have.

   #+BEGIN_question
   Adapt the ~iperf3~ example to provide a command line
    - Either using G5k physical machines:
      #+BEGIN_SRC bash
      # deploy the dependencies of the experimentation using the G5k provider
      myperf g5k

      # launch a performance measurement
      # ideally exposes all the iperf3 client options there ;)
      myperf bench -t 120

      # Backup the reports / influxdb database
      myperf backup
      #+END_SRC

    - Either using the virtual machines on Grid'5000:
      #+BEGIN_SRC bash
      # deploy the dependencies of the experimentation using the G5k provider
      myperf vm5k

      # Subsequent command line should be the same as above
      # enjoy :)
      #+END_SRC
   #+END_question

